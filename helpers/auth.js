module.exports = {
    checkAuthenticated: function(req, res, next){
        if(req.isAuthenticated()){
            return next();
        } 
        res.redirect('/');
    },
    checkGuest: function(req, res, next){
        if(req.isAuthenticated()){
            res.redirect('/dashboard');
        } else{
            return next();
        }
    }
}