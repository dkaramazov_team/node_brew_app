const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const User = mongoose.model('users');
const Post = mongoose.model('posts');

const { checkAuthenticated, checkGuest } = require('../helpers/auth');

// get by user 
router.get('/user/:userId', (req, res) => {
    Post.find({ user: req.params.userId, status: 'public' })
        .populate('user')
        .then(posts => {
            res.render('posts/index', {
                posts: posts
            });
        })
        .catch(error => console.log(error));
});

// logged in user's posts
router.get('/my', checkAuthenticated, (req, res) => {
    Post.find({ user: req.user.id })
        .populate('user')
        .then(posts => {
            res.render('posts/index', {
                posts: posts
            });
        })
        .catch(error => console.log(error));
});

// index
router.get('/', (req, res) => {
    Post.find({ status: 'public' })
        .populate('user')
        .sort({ date: 'desc' })
        .then(posts => {
            res.render('posts/index', {
                posts: posts
            });
        })
        .catch(error => {
            console.log(error);
        });
});

// delete
router.delete('/', (req, res) => {
    Post.remove({ _id: req.params.id })
        .then(() => {
            redirect('/dashboard');
        })
        .catch(error => console.log(error));
});

router.get('/show/:id', (req, res) => {
    Post.findOne({
        _id: req.params.id
    })
        .populate('user')
        .populate('comments.commentUser')
        .then(post => {
            if (post.status == 'public') {
                res.render('posts/show', {
                    post: post
                });
            } else {
                if (req.user) {
                    if (req.user.id == post.user._id) {
                        res.render('posts/show', {
                            post: post
                        });
                    } else {
                        res.redirect('/posts');
                    }
                } else {
                    res.redirect('/posts');
                }
            }
        });
});

// edit post
router.put('/:id', (req, res) => {
    Post.findOne({
        _id: req.params.id
    })
        .populate('user')
        .populate('comments.commentUser')
        .then(post => {
            post.title = req.body.title;
            post.body = req.body.body;
            post.status = req.body.status;
            post.allowComments = req.body.allowComments ? true : false;

            post.save()
                .then(post => {
                    res.redirect('/dashboard');
                })
                .catch(error => console.log(error));
        })
        .catch(error => console.log(error));
});

// get edit form
router.get('/edit/:id', checkAuthenticated, (req, res) => {
    Post.findOne({
        _id: req.params.id
    })
        .then(post => {
            if (post.user != req.user.id) {
                res.redirect('/posts');
            } else {
                res.render('posts/edit', {
                    post: post
                });
            }
        });
});


// post
router.post('/', (req, res) => {
    const newPost = {
        title: req.body.title,
        body: req.body.body,
        status: req.body.status,
        allowComments: req.body.AllowComments ? true : false,
        user: req.user.id
    };

    new Post(newPost)
        .save()
        .then(post => {
            res.redirect(`posts/show/${post.id}`);
        })
        .catch(error => {
            console.log(error);
        });
});

// add form
router.get('/add', checkAuthenticated, (req, res) => {
    res.render('posts/add');
});

// add comment
router.post('/comment/:id', (req, res) => {
    Post.findOne({ _id: req.params.id })
        .then(post => {
            const newComment = {
                commentBody: req.body.commentBody,
                commentUser: req.user.id
            }
            post.comments.unshift(newComment);
            post.save()
                .then(post => {
                    res.redirect(`/posts/show/${post.id}`);
                });
        });
});

module.exports = router;